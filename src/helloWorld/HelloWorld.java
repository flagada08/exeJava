package helloWorld;

public class HelloWorld {
/**
 * 
 * @param args
 */
	public static void main(String[] args) {
		// This is a comment
		/* The code below will print the words Hello World
		to the screen, and it is amazing */
		System.out.println("Hello World!");
		System.out.println("I am learning Java.");
		System.out.println("It is awesome!");
		System.out.println(3 + 3); // This is a comment
		System.out.print("Hello World! ");
		System.out.println("I will print on the same line.");
		
		String name = "John";
		System.out.println(name);
		
		int myNum = 15;
		System.out.println(myNum);
		
		int myNum1 = 15;
		myNum1 = 20;  // myNum is now 20
		System.out.println(myNum1);
		
		// final int myNum11 = 15;
		// myNum11 = 20;  // will generate an error: cannot assign a value to a final variable
		
		int myNum11 = 5;             // Integer (whole number)
		float myFloatNum = 5.99f;    // Floating point number
		char myLetter = 'D';         // Character
		boolean myBool = true;       // Boolean
		String myText = "Hello";     // String
		
		System.out.println("int " + myNum11);
		System.out.println("float " + myFloatNum);
		System.out.println("char " + myLetter);
		System.out.println("boolean " + myBool);
		System.out.println("String " + myText);
		
		String name1 = "John";
		System.out.println("Hello " + name1);
		
		String firstName = "John ";
		String lastName = "Doe";
		String fullName = firstName + lastName;
		System.out.println(fullName);
		
		int x = 5;
		int y = 6;
		System.out.println(x + y); // Print the value of x + y
		
		int x1 = 5;
		int y1 = 6;
		int z = 50;
		System.out.println(x1 + y1 + z);
		
		int x11 = 5, y11 = 6, z1 = 50;
		System.out.println(x11 + y11 + z1);
		
		int x111, y111, z11;
		x111 = y111 = z11 = 50;
		System.out.println(x111 + y111 + z11);
		
		// Good
		@SuppressWarnings("unused")
		int minutesPerHour = 60;

		// OK, but not so easy to understand what m actually is
		@SuppressWarnings("unused")
		int m = 60;
		
		byte myNum111 = 100;
		System.out.println(myNum111);
		
		short myNum1111 = 5000;
		System.out.println(myNum1111);
		
		int myNum11111 = 100000;
		System.out.println(myNum11111);
		
		long myNum111111 = 15000000000L;
		System.out.println(myNum111111);
		
		// A floating point number can also be a scientific number with an "e" to indicate the power of 10:
		float f1 = 35e3f;
		double d1 = 12E4d;
		System.out.println(f1);
		System.out.println(d1);
		
		boolean isJavaFun = true;
		boolean isFishTasty = false;
		System.out.println(isJavaFun);     // Outputs true
		System.out.println(isFishTasty);   // Outputs false
		
		char myVar1 = 65, myVar2 = 66, myVar3 = 67;
		System.out.println("ASCII value 65: " + myVar1);
		System.out.println("ASCII value 66: " + myVar2);
		System.out.println("ASCII value 67: " + myVar3);
	}

}
